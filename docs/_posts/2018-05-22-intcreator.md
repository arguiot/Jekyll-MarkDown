---
title: Tim Berners-Lee, the creator of the Internet
layout: post
categories: Technology
---

# Who is Tim Berners-Lee?

**Tim Berners-Lee** is a famous man known for being the inventor of the Internet in its current form. He leads the development of the World Wide Web as the director of the World Wide Web Consortium (W3C). He has been anoblished by Queen Elizabeth II of England.
![Tim Berners-Lee](https://upload.wikimedia.org/wikipedia/commons/4/4e/Sir_Tim_Berners-Lee_%28cropped%29.jpg)

## Before the Internet
He was born in London, England, in 1955. His parents worked for the 1st commercial computer. Meanwhile, Tim Berners-Lee would learn all about electronics through his train model, being fascinated by its internal workings. When he became an adult, he would be an engineer at Plessey (a telecom company) and went with D.G. Nash to become a programmer who would create printing pilots. In 1980, he becomes a contractor for the CERN, where he proposed something that would become a very important part of the Internet: hypertext, some kind of text which can be displayed between computers (being shared) through a prototype named ENQUIRE. Between 1980 and 1984, he becomes the technical engineer in an English company in which he invents *Realtime RPC*, basically a way for a program to execute itself in another address space in a networking component like a local program without the developer saying it explicitly.


## Return to CERN - The invention of the Internet
Tim Berners-Lee returns to CERN as a *fellow* and he would start uniting various projects: hypertext, objects, domain name system, TCP... to make the Internet. He would make his own web browser, the first one which will exist, and the first web server through an HTTPd (HyperText Transfer Protocol Deamon).

![First computer with Internet](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d1/First_Web_Server.jpg/800px-First_Web_Server.jpg)

The first web site will exist at the CERN, at the address [http://info.cern.ch/hypertext/WWW/TheProject.html](http://info.cern.ch/hypertext/WWW/TheProject.html) (still exists) running on the first HTTP deamon on NeXT OS. This website was dedicated to explain how to use the Web. Berners-Lee explained this would allow sharing of informations and everything else, and the 25 greatest scientists declared the Internet will become the greatest invention of all times (they were right), saying we can connect with everyone, everywhere and anytime.

## The results of the creation of the Internet
In 1994, he would create the World Wide Consortium (W3C), an organization creating standards for the web. His main goal for this was to make the Internet free so that people would gradually switch to Internet. He believed fees would hinder the Internet's adoption by the people.
In 2001, he will create the Semantic Web (a version of WWW defined for interoperability between services). He would also become a member of the Royal Society.
In 2004, he becomes a Sire, a rank in the British nobility slightly superior to the rank of Duke. He would later be part of the Order of Merit in 2007.
In 2009, he admits that adding double slashes (//) in web addresses were a mistake: he used it as a distinction for different protocols, but it became mostly useless with web browsers defaulting to HTTP and HTTPS. He will also create the WWW Foundation to help develop the web in the goal of changing the world.
In 2013, he will lead the Alliance for Affordable Internet, an alliance between Internet giants to make Internet access cheaper for the people who would not be able to pay for the Internet, since ISPs **are** abusing of their control through their prices and monopoly.
In 2017, he would become one of the leading people in favour for Net Neutrality in its fight against Chairman Pai of the FCC in the United States.

## Conclusion

Tim Berners-Lee was a great man who made our world better, especially the numeric world, and without him we wouldn't write here on the Internet and you wouldn't read our articles. 🙂
