---
title: Open-source licenses
layout: post
categories: Miscellaneous
---
# Open-source licenses

Most people are not aware of almost content creator have to know everyday: what is _open-source_.
These same people happen to be the ones who only have one computer with Internet Explorer and Microsoft Office...
Now, we will explain to you what it is.

## Meaning
Open-source means that the published content, whether it's a:
- video
- image
- music
- software

has been created by someone who chose to waive some of his rights over their creation, notably their right to control how their content is being distributed or reused for other purposes.
So, following the terms of the open-source license, you could have the right to use that same content without being prevented of doing so.
Of course, it depends on what that particular license allows you to do.


There are general licenses and specific licences, tailored to every type of content and your own needs.
For example, you could only allow sharing or you could authorize anyone to reuse your content, even for commercial reasons.
![Creative Commons Licenses](https://www.icnsoft.org/wp-content/uploads/2018/04/creative-commons-783531_1280.png)
Logos of Creative Commons Licenses most commonly found


## Comparison
Here is a comparison table:

| License name                                                | Content usage conditions                                                                                                                                                |
| --------                                                    |---                                   															|
| Mozilla Public License                                      | Code: you have the right to reuse some or all of a program licensed under the MPL, as long as you are **not** using the same brand                 	|
|----							      |---																			|
| Berkeley Software Distribution License                      | Code: you can reuse some of all of a program but you cannot use the same brand and you might need to request a patent                                   |
|----							      |---																			|
| Creative Commons Attribution+ShareAlike                     | Images, videos and music: you can reuse their content but you must give proper credit and share it in a way that your new content is similar to the original |
|----							      |---																			|
| Creative Commons Non-Commercial                             | You can reuse the content in any way, but you are forbidden to use it for commercial purposes  								|
|----                                                         |---																			|
| Public Domain or CC0                                        | You may use it for any reasons, as the owner waived his ownership. However, it's now the entire public who owns it 					|
|----                                                         |---																			|
| GNU Affero General Public License                           | It's pure copyleft, do not use it for commerical purposes 												|
|----                                                         |---															
| Apache License                                              | You may reuse it but brands cannot be used
|----                                                         |---
| Creative Commons Attribution+NonCommerical+NonDerivatives   | Your sole right with this content is to share it |
|----                                                         |---
|---                                                          |---
{: rules="groups"}

## Advantages
The best perk with Creative Commons is that you can choose all the options or leave some:
- You can allow or disallow commercial purposes
- Allow or deny Modification and state how you wish people can change your creation
- Allow or deny sharing and broadcasting


The organization Creative Commons has put in place a license selector guiding you through your needs and goals: [CreativeCommons.org](https://creativecommons.org/choose/)
