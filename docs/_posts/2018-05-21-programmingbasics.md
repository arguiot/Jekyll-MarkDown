---
title: How to learn the basics of programming
layout: post
categories: Programming
---

# How to learn programming basics

## Introduction

Programming is something unique and special, which is also the income of an entire industry. 
So, many readers asked us: how can we learn programming? There is a way that might be useful to you.

### The language

There are more than a hundred different programming languages, some are specialized in specific uses. 
Depending on those, you’ll select a language fitting your needs (do you need to program a video game? 
a robot? a website?), because possibilities are unlimited. You will then need to start finding one. 

However, I would advise you to concentrate on a particular programming language instead of learning 
a bit of each. If you want to make a robot, I would recommend you using LabView or Arduino (this one is better because it's free). 

If you want to make a game, then I would recommend you using Python (with the PyGame library or Matplotlib) 
or C (with all its sub-languages like C-Sharp, C++)

### The programming medium

You will also need to be in a comfortable environment, physically and visually speaking:

Install an Integrated Development Environment (known also as an IDE) with syntax highlighting, 
with themes so that your eyes will be more accustomed to it to your liking when writing a program. 
You could install Visual Studio Community, a free Microsoft-based development suite derived from Visual Studio, 
comfortable to manipulate, that can do almost every common languages; Python IDLE is also a 
good choice, and comes as a bundle with Python when downloading it from the Python Software Foundation website.
![Visual Studio Community IDE](https://www.visualstudio.com/wp-content/uploads/2018/05/visual-studio-ide-1200x683.png)
Physically speaking, I would recommend you to be in a room with a comfortable desk, a keyboard 
with minimal resistance when keys are pressed, and has to be mechanical if possible because it is harder 
to break than a regular keyboard, an ergo mouse, a Night light-adaptative screen to prevent eye damage and 
insomnia because of the blue lignt, and a leather chair adapting itself to your body.

### Where to learn?

Learning to program might be difficult, because we don’t always know how to start. You could always 
hire a teacher, but before, let’s find free options.

(Almost) Every programming language has a precise and complete documentation describing every function 
in that particular programming language. You will learn from there a lot about the programming language 
you began to learn, and often tutorials are included.

There are also free online resources like OpenClassrooms, FreeCodeCamp, CodeCademy, Code.org (only for Javascript), France-IOI or Scratch by the MIT (C++). They’ll guide you in your programming skills and how to develop them with interactive exercises.
![Scratch IDE](https://i.ytimg.com/vi/t8E44wzZtRs/maxresdefault.jpg)
### How to learn?

To start, you might find interesting to learn coding through blocks to understand computers’ and programming logics. 
In that case, tools like Code.org, mBlock, Scratch, Fritzing (to verify) might help you. By teaching you programming 
in their specialization, block-based coding will help you a lot.

When this is done, you can gradually learn real coding – this might take a lot of time. Complete all courses online 
and in real life with maximum attention and being really motivated.

There is a lot to learn for a programming language, and everything can be forgotten very easily.

I also encourage you to expand your programming skills and knowledge by adding new code libraries (libs) to help you 
and learn how to code with them for a better program.

Practise every week at least two hours and watch out for syntax!
