<h1 align="center">Jekyll-Markdown</h1>

**Jekyll-Markdown** is a fancy **markdown blog** generator using jekyll. All your blog posts are edited in pure **MarkDown** and the blog is responsive on any device.

# Getting Started

You can view the template website at: [https://lukalafaye.github.io/Jekyll-MarkDown/](https://lukalafaye.github.io/Jekyll-MarkDown/)

# Prerequisites

## Installing git

Depending on your OS, follow the steps at: **[https://git-scm.com/book/en/v2/Getting-Started-Installing-Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)**

## Installing Jekyll

Depending on your OS, follow the steps at: **[https://jekyllrb.com/docs/installation/](https://jekyllrb.com/docs/installation/)**

# Installing

1. **Clone the whole repository from github**

This can be done by typing this in a **Terminal** / GIT **CMD**

```
git clone https://github.com/lukalafaye/Jekyll-MarkDown/
```

# Deployment

## Running jekyll to make a static web server

**To build and make a static server, type this command inside Jekyll-Markdown folder:**

```
jekyll s
```

**Finally, check that the web server is running by typing this address in your web browser:**

```
localhost:4000
```

## Built With

* **[Jekyll](https://jekyllrb.com/) - Static Site Generator**
* **[MarkDown](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) - Lightweight Markup Language**

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

Lucas Gruwez
* **Billie Thompson** - *Initial work* - [PurpleBooth](https://github.com/PurpleBooth)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone who's code was used
* Inspiration
* etc
